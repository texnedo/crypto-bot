from unittest import TestCase
from src.crypto_price_provider import CryptoPriceProvider


class TestCryptoPriceProvider(TestCase):
    def test_get_price(self):
        provider = CryptoPriceProvider()
        price = provider.get_price(CryptoPriceProvider.CRYPTO_ETHERIUM,
                                   CryptoPriceProvider.FIAT_EUR)
        self.assertIsNotNone(price)
        price1 = provider.get_price(CryptoPriceProvider.CRYPTO_ETHERIUM,
                                    CryptoPriceProvider.FIAT_EUR)
        self.assertEqual(price, price1)
        price3 = provider.get_price(CryptoPriceProvider.CRYPTO_ETHERIUM,
                                    CryptoPriceProvider.FIAT_USD)
        self.assertIsNotNone(price3)
        price4 = provider.get_price(CryptoPriceProvider.CRYPTO_BITCOIN,
                                    CryptoPriceProvider.FIAT_RUR)
        self.assertIsNotNone(price4)
        price5 = provider.get_price(CryptoPriceProvider.CRYPTO_ETHERIUM,
                                    CryptoPriceProvider.FIAT_RUR)
        self.assertIsNotNone(price5)
