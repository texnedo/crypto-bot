import asyncio
import time
from tarantool import Connection
from unittest import TestCase
from tarantool_queue import Queue
import threading

exceptions = []
data = []


def process_task():
    try:
        queue = Queue("localhost", 3301, 0)
        tube = queue.tube("q4")
        while True:
            task_collected = tube.take(timeout=10)
            print(task_collected.raw_data)
            data.append(task_collected.raw_data)
            task_collected.ack()
            if len(data) == 200:
                break
    except Exception as ex:
        exceptions.append(ex)


def writer_task():
    try:
        queue = Queue("localhost", 3301, 0)
        tube = queue.tube("q4")
        name = threading.current_thread().name
        for i in range(0, 100, 1):
            tube.put((i, name, 'test'), delay=3)
            if i % 5 == 0:
                print(name + ' start sleep')
                time.sleep(0.1)
    except Exception as ex:
        exceptions.append(ex)


class TestUseTarantoolQueue(TestCase):
    def test_queue(self):
        th = threading.Thread(target=process_task)
        th.start()

        th1 = threading.Thread(target=writer_task, name='wr1')
        th1.start()

        th2 = threading.Thread(target=writer_task, name='wr2')
        th2.start()

        th.join(15)
        th1.join(15)
        th2.join(15)
        self.assertFalse(th.is_alive())
        self.assertFalse(th2.is_alive())
        self.assertFalse(th2.is_alive())
        self.assertTrue(len(exceptions) == 0)
        self.assertTrue(len(data) != 0)
        print(exceptions)
