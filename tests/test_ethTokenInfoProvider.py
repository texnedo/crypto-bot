from unittest import TestCase
from src.eth_token_info_provider import EthTokenInfoProvider


class TestEthTokenInfoProvider(TestCase):
    def test_get_token_balance(self):
        provider = EthTokenInfoProvider()
        balance = provider.get_token_balance('0x8b114487CA0e566a1C4ABc16b757bFAdE342728A')
        self.assertIsNotNone(balance)
        balance1 = provider.get_token_balance('0x8b114487CA0e566a1C4ABc16b757bFAdE342728A')
        self.assertEqual(balance, balance1)

    def test_get_token_balance_incorrect(self):
        provider = EthTokenInfoProvider()
        self.assertRaises(Exception, provider.get_token_balance, 'adsfasdfasdf')
