from threading import Thread
from time import sleep
from unittest import TestCase
from scheduled_action_manager import ScheduledActionManager, ChatActionType, ServiceActionType, \
    ActionSubscriber
from scheduled_action_manager import ServiceAction
from scheduled_action_manager import ChatAction


class TestScheduledActionManager(TestCase, ActionSubscriber):
    actions = list()

    def process_action(self, action):
        self.actions.append(action)
        return True

    def test_connect(self):
        manager = ScheduledActionManager()
        manager.add_subscriber(self)
        sleep(5)
        manager.stop(None)

    def test_add_actions(self):
        self.actions.clear()
        manager = ScheduledActionManager()
        manager.add_subscriber(self)
        action1 = ChatAction()
        action1.chat_id = 10
        action1.chat_action_type = ChatActionType.MINER_REPORT
        action1.params_json = '{"some":1}'
        action1.delay = 3
        action1.ttl = 1000
        manager.add_action(action1)
        action2 = ServiceAction()
        action2.service_action_type = ServiceActionType.UNKNOWN
        action2.params_json = '{"some":2}'
        action2.delay = 5
        action2.ttl = 1000
        manager.add_action(action2)
        sleep(10)
        manager.remove_subscriber(self)
        self.assertEqual(len(self.actions), 2)
        action_check1 = self.actions[0]
        action_check2 = self.actions[1]
        self.assertEqual(action_check1.chat_id, 10)
        self.assertEqual(action_check2.params_json, '{"some":2}')
        manager.stop(None)

    def test_add_actions_different_threads(self):
        self.actions.clear()
        manager = ScheduledActionManager()
        manager.add_subscriber(self)
        action1 = ChatAction()
        action1.chat_id = 10
        action1.chat_action_type = ChatActionType.MINER_REPORT
        action1.params_json = '{"some":1}'
        action1.delay = 3
        action1.ttl = 1000
        th1 = Thread(target=lambda: manager.add_action(action1))
        th1.start()
        action2 = ServiceAction()
        action2.service_action_type = ServiceActionType.UNKNOWN
        action2.params_json = '{"some":2}'
        action2.delay = 5
        action2.ttl = 1000
        th2 = Thread(target=lambda: manager.add_action(action2))
        th2.start()
        sleep(10)
        manager.remove_subscriber(self)
        self.assertEqual(len(self.actions), 2)
        action_check1 = self.actions[0]
        action_check2 = self.actions[1]
        self.assertEqual(action_check1.chat_id, 10)
        self.assertEqual(action_check2.params_json, '{"some":2}')
        manager.stop(None)
