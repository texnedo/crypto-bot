from unittest import TestCase
from bot_storage import BotStorage
from bot_chats_manager import BotChatsManager


class TestBotChatsManager(TestCase):
    def test_get_chat(self):
        storage = BotStorage()
        bot_chats_manager = BotChatsManager(storage)
        bot_chats_manager.remove_chat(5)
        self.assertIsNone(bot_chats_manager.get_chat(5))
        chat = bot_chats_manager.update_chat(5, 'private', '{}')
        self.assertIsNotNone(chat)
        chat = bot_chats_manager.update_chat_settings(chat, '{"key":"value"}')
        self.assertIsNotNone(chat)
        self.assertEqual(chat.settings_json, '{"key":"value"}')
