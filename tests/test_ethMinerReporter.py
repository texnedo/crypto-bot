from unittest import TestCase
from eth_miner_reporter import EthMinerReporter


class TestEthMinerReporter(TestCase):
    def test_make_text_report(self):
        reporter = EthMinerReporter()
        report = reporter.make_text_report('0x8b114487CA0e566a1C4ABc16b757bFAdE342728A')
        self.assertIsNotNone(report)
