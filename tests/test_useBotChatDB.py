from datetime import date, datetime, timedelta

from utils.init_helper import get_config
import MySQLdb
import hashlib
from unittest import TestCase
from bot_storage import BotStorage
import time

config = get_config()


class TestUseBotChatDb(TestCase):
    def test_insert_many_records(self):
        storage_config = config['bot_storage']
        db_config = storage_config['mysql_db_credentials']

        db = MySQLdb.connect(
            host=db_config['host'],
            user=db_config['user'],
            passwd=db_config['password'],
            db=db_config['db_name']
        )

        cursor = db.cursor()
        cursor.execute('delete from bot_chats')
        db.commit()

        for i in range(1, 1000000):
            statement = 'insert into bot_chats (chat_id, chat_type, created_ts, ' \
                        'last_active_ts, eth_miner_token, ' \
                        'chat_info_json, settings_json) ' \
                        'values (%s, %s, %s, %s, %s, %s, %s);'
            data = (i, 'private', datetime.now().date(), datetime.now().date() + timedelta(days=1),
                    hashlib.sha256(('token' + str(i)).encode('utf-8')).hexdigest(),
                    '{{"foo{}":"bar{}"}}'.format(i, i), '{{"some{}":"staff{}"}}'.format(i, i))
            cursor.execute(statement, data)
            if i % 1000 == 0:
                db.commit()

        cursor.close()
        db.close()

    def test_select_all_records(self):
        storage_config = config['bot_storage']
        db_config = storage_config['mysql_db_credentials']

        db = MySQLdb.connect(
            host=db_config['host'],
            user=db_config['user'],
            passwd=db_config['password'],
            db=db_config['db_name']
        )

        cursor = db.cursor()
        cursor.execute('select * from bot_chats where chat_id = 10')
        result = cursor.fetchall()
        for item in result:
            print(item)
            print(item[1])
        cursor.close()
        db.close()

    def test_bot_storage_api(self):
        storage = BotStorage()
        storage.remove_chat_info(1)
        info = storage.get_chat_info(1)
        self.assertIsNone(info)
        storage.remove_chat_info(1)
        storage.update_chat_info(1, 'private', '{"creator":"test"}')
        info1 = storage.get_chat_info(1)
        self.assertIsNotNone(info1)
        self.assertEqual(info1[1], 'private')
        self.assertEqual(info1[5], '{"creator":"test"}')
        storage.update_chat_info(1, 'private', '{"creator":"test", "some":"name"}')
        info2 = storage.get_chat_info(1)
        self.assertEqual(info2[5], '{"creator":"test", "some":"name"}')
        storage.update_eth_miner_token(1, '0x43421341wertwertwer')
        info3 = storage.get_chat_info(1)
        self.assertEqual(info3[4], '0x43421341wertwertwer')
        storage.update_chat_settings(1, '{"some":"test"}')
        info4 = storage.get_chat_info(1)
        self.assertEqual(info4[6], '{"some":"test"}')
        info5 = storage.get_chat_info_by_token('0x43421341wertwertwer')
        self.assertIsNotNone(info5)
