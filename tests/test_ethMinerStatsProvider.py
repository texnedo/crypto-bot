from unittest import TestCase
from src.eth_miner_provider import EthMinerStatsProvider


class TestEthMinerStatsProvider(TestCase):
    def test_get_miner_stats(self):
        provider = EthMinerStatsProvider()
        stats = provider.get_miner_stats('0x8b114487CA0e566a1C4ABc16b757bFAdE342728A')
        self.assertIsNotNone(stats)
        stats1 = provider.get_miner_stats('0x8b114487CA0e566a1C4ABc16b757bFAdE342728A')
        self.assertEqual(stats, stats1)
