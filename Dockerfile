FROM python:alpine3.7
COPY ./src /app
WORKDIR /app
RUN set -e; \
	apk add --no-cache --virtual .build-deps \
		gcc \
		libc-dev \
		linux-headers \
		mariadb-dev \
		py-mysqldb \
		python3-dev \
		build-base \
	; \
	apk add --no-cache mariadb-client-libs; \
	pip install -r requirements.txt; \
	apk del .build-deps;
RUN pip install -r requirements.txt
CMD python ./crypto_bot_telegram.py