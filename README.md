# README #

### Project ###
Crypto bot for Telegram messenger.

### Note ###
This is a self educational project to play with server side technologies and do something valuable for myself. 

## Motivation ##
In the beginning of 2018 I was interested in Crypto Currency tech (as probably lots of people) and tried to get deeper into it, experimented with different stuff including mining process.
This project is intended to be small help for individual miners who want to use Telegram messenger as a way to control their mining process and raised funds.

## Tech stack ##
1. Python, python-telegram-bot
2. Mysql to store user's profiles and some meta information
3. tarantool, tarantool-queue to build fast time based queue for a miner report scheduling
4. Docker to build and deploy all that to a cloud host 
