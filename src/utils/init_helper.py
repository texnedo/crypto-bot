import logging
import yaml
import sys, signal, os
import socks
import socket
import optparse

config = None

p = optparse.OptionParser()
p.add_option('--proxy_url', '-u')
p.add_option('--proxy_user', '-s')
p.add_option('--proxy_pass', '-a')
p.add_option('--bot_token', '-t')
p.add_option('--mysql_user', '-m')
p.add_option('--mysql_pass', '-p')
options, arguments = p.parse_args()
options_dict = vars(options)


def constructor_test(loader, node, suffix):
    return options_dict[node]


def get_config():
    global config
    if config is None:
        with open('config.yaml') as file:
            yaml.add_multi_constructor(tag_prefix='!', multi_constructor=constructor_test)
            config = yaml.load(file.read())
        if config is None:
            raise IOError('Filed to load config file (config.yaml)')
    return config


def get_logger(log_tag):
    if not log_tag:
        raise IOError('Calling component must provide logging tag')

    config = get_config()
    logger = logging.getLogger(log_tag)
    log_level = config['log_level']
    logger.setLevel(logging.getLevelName(log_level))
    # create file handler which logs even debug messages
    fh = logging.FileHandler(config['log_file'])
    fh.setLevel(log_level)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(log_level)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger


def set_signal_handler(handler):
    signal_codes = {signal.SIGINT, signal.SIGABRT, signal.SIGTERM}
    for signal_code in signal_codes:
        signal.signal(signal_code, handler)


def install_proxy_rewrite(host, ip):
    socket.getaddrinfo = lambda *args: \
        [(socket.AF_INET, socket.SOCK_STREAM, 6, '',
          (args[0] == host and ip or args[0], args[1]))]
