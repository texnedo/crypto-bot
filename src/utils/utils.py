import json
from collections import namedtuple


def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())


def json2obj_hooked(data):
    return json.loads(data, object_hook=_json_object_hook)


def json2obj(data):
    return json.loads(data)


def obj2json(data):
    return json.dumps(data)


def eth_token_from_hash(eth_hash):
    if eth_hash is None or len(eth_hash) == 0:
        return eth_hash
    if eth_hash.startswith('0x'):
        return eth_hash
    return '0x' + eth_hash


def read_from_file(file_path):
    file = open(file_path, 'r')
    content = file.read()
    file.close()
    return content
