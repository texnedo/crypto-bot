from utils.init_helper import get_logger, set_signal_handler
from utils.init_helper import get_config
from threading import Thread, Lock, current_thread, Event, RLock
from time import sleep
from tarantool import Connection
from tarantool_queue import Queue
from enum import Enum

# TODO - remove hardcoded log tag
logger = get_logger('scheduled_action_manager')
config = get_config()

action_manager_config = config['action_manager']
tarantool_config = action_manager_config['tarantool_credentials']

THREAD_JOIN_TIMEOUT = action_manager_config['thread_join_timeout']
QUEUE_TAKE_TIMEOUT = action_manager_config['queue_take_timeout']
THREAD_WAIT_CHECK_EVENT = action_manager_config['thread_check_wait_timeout']
THREAD_MAX_ERROR_COUNT = action_manager_config['thread_max_error_count']

action_managers = set()


def signal_handler(signum, frame):
    logger.log('handle signal: {} with frame: {}, managers: {}'
               .format(signum, frame, len(action_managers)))
    for action_manager in action_managers:
        action_manager.stop(signum)


set_signal_handler(signal_handler)


class ScheduledActionType(Enum):
    SERVICE = 1
    CHAT = 2


class ChatActionType(Enum):
    MINER_REPORT = 1


class ServiceActionType(Enum):
    UNKNOWN = 0


class ScheduledActionBase:
    def __init__(self):
        self.type = None
        self.delay = int(action_manager_config['default_delay'])
        self.ttl = int(action_manager_config['default_ttl'])
        self.params_json = None

    def pack_task_data(self):
        raise Exception('This method must be overridden in a descendant class')


class ChatAction(ScheduledActionBase):
    def __init__(self, task_data=None):
        super().__init__()
        self.type = ScheduledActionType.CHAT
        #TODO - make sure that ttl less delay
        if task_data is None:
            self.chat_id = None
            self.chat_action_type = None
        else:
            if len(task_data) != 4:
                raise Exception('Illegal chat action data: {}'.format(task_data))
            self.chat_action_type = ChatActionType(task_data[1])
            self.chat_id = task_data[2]
            self.params_json = task_data[3]

    def pack_task_data(self):
        data = (self.type.value, self.chat_action_type.value, self.chat_id, self.params_json)
        return data


class ServiceAction(ScheduledActionBase):
    def __init__(self, task_data=None):
        super().__init__()
        self.type = ScheduledActionType.SERVICE
        if task_data is None:
            self.service_action_type = ServiceActionType.UNKNOWN
        else:
            if len(task_data) != 3:
                raise Exception('Illegal service action data: {}'.format(task_data))
            self.service_action_type = ServiceActionType(task_data[1])
            self.params_json = task_data[2]

    def pack_task_data(self):
        data = (self.type.value, self.service_action_type.value, self.params_json)
        return data


class ActionSubscriber:
    def process_action(self, action):
        raise Exception('This method must be overridden in a descendant class')


class ScheduledActionManager:
    def __init__(self):
        self.subscribers = set()
        self.fetch_action_queue = None
        self.fetch_action_tube = None
        self.lock_add_action_tube = RLock()
        self.add_action_queue = None
        self.add_action_tube = None
        self.thread = Thread(target=self.fetch_ready_items, name='fetch_ready_items_worker')
        self.event_stop = Event()
        self.is_thread_started = False
        action_managers.add(self)

    def __del__(self):
        self.stop(None)
        action_managers.remove(self)

    def stop(self, signum):
        if self.thread is None or not self.thread.is_alive():
            return
        try:
            self.event_stop.set()
            self.thread.join(THREAD_JOIN_TIMEOUT)
            if self.thread.is_alive():
                logger.error('Failed to wait for thread %s join (is alive: %s)',
                             self.thread, self.thread.is_alive())
            else:
                if signum is not None:
                    logger.info('Thread %s successfully joined after signal %d',
                                self.thread, signum)
                else:
                    logger.info('Thread %s successfully joined', self.thread)
        except Exception as ex:
            logger.error('Failed to wait for thread %s join due to: %s', self.thread, ex)
        self.thread = None

    def get_fetch_action_tube(self):
        if self.fetch_action_tube is None:
            try:
                self.fetch_action_queue = Queue(tarantool_config['host'],
                                                tarantool_config['port'],
                                                0)
                self.fetch_action_tube = self.fetch_action_queue.tube(
                    name=action_manager_config['action_queue_name'],
                    ttl=int(action_manager_config['default_ttl'])
                )
            except Exception as ex:
                logger.error('Failed to initialize fetch action queue due to an error: %s', ex)
        return self.fetch_action_tube

    def get_add_action_tube(self):
        try:
            self.lock_add_action_tube.acquire()
            if self.add_action_tube is None:
                try:
                    self.add_action_queue = Queue(tarantool_config['host'], tarantool_config['port'], 0)
                    self.add_action_tube = self.add_action_queue.tube(
                        name=action_manager_config['action_queue_name'],
                        ttl=int(action_manager_config['default_ttl'])
                    )
                except Exception as ex:
                    logger.error('Failed to initialize add action queue due to an error: %s', ex)
            return self.add_action_tube
        finally:
            self.lock_add_action_tube.release()

    def fetch_ready_items(self):
        logger.info('Start fetch loop in thread: %s', current_thread())
        error_count = 0
        while not self.event_stop.isSet():
            task = None
            try:
                tube = self.get_fetch_action_tube()
                task = tube.take(QUEUE_TAKE_TIMEOUT)
                if task is not None:
                    if self.process_task(task):
                        task.ack()
                    else:
                        task.bury()
                        logger.error('Failed to process task %s', task)
                error_count = 0
            except Exception as ex:
                error_count += 1
                logger.error('An error in fetch loop (%d) in thread: %s, %s',
                             error_count, current_thread(), ex)
                try:
                    if task is not None:
                        task.bury()
                except Exception as ex_internal:
                    logger.error('Failed to bury task %s due to error: %s', task, ex_internal)
                    error_count += 1
                if error_count > THREAD_MAX_ERROR_COUNT:
                    logger.error('Max error count has been reached in thread: %s, %s',
                                 current_thread(), ex)
                    raise ex
            if task is None and self.event_stop.wait(THREAD_WAIT_CHECK_EVENT):
                logger.info('Stop processing in thread by event: %s', current_thread())
                break
        logger.info('Exit fetch loop in thread: %s', current_thread())

    def process_task(self, task):
        logger.info('Process task: {}'.format(task.raw_data))
        action = self.action_factory(task.raw_data)
        if action is None:
            return True
        result = True
        for subscriber in self.subscribers:
            result &= subscriber.process_action(action)
        return result

    def action_factory(self, task_data):
        type = task_data[0]
        # noinspection PyTypeChecker
        if type <= 0 or type > len(ScheduledActionType):
            raise Exception('Failed to read action type: {}'.format(type))
        action_type = ScheduledActionType(type)
        if action_type == ScheduledActionType.CHAT:
            return ChatAction(task_data)
        elif action_type == ScheduledActionType.SERVICE:
            return ServiceAction(task_data)
        raise Exception('Undefined action type: {}'.format(action_type))

    def add_action(self, action):
        if action is None or action.type is None:
            raise Exception('Illegal action provided: {}'.format(action))
        logger.info('Schedule action: %s', action)
        try:
            self.lock_add_action_tube.acquire()
            tube = self.get_add_action_tube()
            ttl = action.ttl
            delay = action.delay
            if ttl < delay and delay > 0:
                ttl = delay * action_manager_config['ttl_multiplier']
            logger.info('Add action %s with delay: %s and ttl: %s', action, delay, ttl)
            task = tube.put(data=action.pack_task_data(), ttl=ttl, delay=delay)
            logger.info('Scheduled action %s submitted with task %s', action, task)
        finally:
            self.lock_add_action_tube.release()

    def add_subscriber(self, subscriber):
        if subscriber is None or not isinstance(subscriber, ActionSubscriber):
            raise Exception('Subscriber must be non null')
        self.subscribers.add(subscriber)
        if self.thread is not None and not self.is_thread_started:
            self.thread.start()
            self.is_thread_started = True
            logger.info('Worker thread %s started', self.thread)

    def remove_subscriber(self, subscriber):
        if subscriber is None or not isinstance(subscriber, ActionSubscriber):
            raise Exception('Subscriber must be non null')
        self.subscribers.remove(subscriber)
