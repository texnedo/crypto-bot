from utils.init_helper import get_config
from utils.init_helper import get_logger

logger = get_logger('help_loader')
config = get_config()
help_loader_config = config['help_loader']
file_name = help_loader_config['file_name']

help_content = None


def get_help_text():
    global help_content
    if help_content is None:
        logger.debug('help_content is not initialized')
        help_file = open(file_name, 'r')
        get_help_text.help_content = help_file.read()
        help_file.close()
        logger.debug('help_content initialization completed (length: {})'
                     .format(len(get_help_text.help_content)))
    return help_content
