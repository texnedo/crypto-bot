queue = require('queue')
log = require('log')

print('Start configuration')

box.cfg {
    listen = 3301,
    log_level = 6,
    log = 'tarantool.log'
}

print('Grant permissions')

box.once('grant', function()
    box.schema.user.grant('guest', 'read,write,execute', 'universe')
    print('Guest user permissions granted')
end)

print('Configuration completed')

existing = queue.tube('action_queue')

local function otc_cb(task, stats_data)
    if stats_data == 'delete' then
        log.info("Task %s is done", task)
    end
end

if not existing then
    print('No exising queue found, create new')
    existing = queue.create_tube('action_queue', 'fifottl', {temporary = false, on_task_change = otc_cb})
end
queue.statistics('action_queue')
print(string.format('Bot queue created: %s', existing))
