from utils.init_helper import get_logger
from utils.init_helper import get_config
from bot_storage import BotStorage

logger = get_logger('bot_chats_manager')
config = get_config()


class BotChat:
    def __str__(self) -> str:
        return '{{chat_id: {}, chat_type: {}}}'.format(self.chat_id, self.chat_type)

    def __init__(self, record) -> None:
        self.chat_id = record[0]
        self.chat_type = record[1]
        self.created_ts = record[2]
        self.last_active_ts = record[3]
        self.eth_miner_token = record[4]
        self.chat_info_json = record[5]
        self.settings_json = record[6]


class BotChatsManager:
    def __init__(self, storage: BotStorage) -> None:
        self.storage = storage
        self.chats = {}

    def get_chat(self, chat_id):
        existing = self.chats.get(chat_id)
        if existing is not None:
            logger.info('chat {} extracted from cache {}'.format(chat_id, existing))
            return existing
        result = self.storage.get_chat_info(chat_id)
        logger.info('chat {} queried from storage'.format(chat_id))
        if result is None:
            return None
        chat = BotChat(result)
        self.chats[chat_id] = chat
        return chat

    def remove_chat(self, chat_id):
        logger.info('remove chat {}'.format(chat_id))
        self.storage.remove_chat_info(chat_id)
        self.chats.pop(chat_id, None)

    def update_chat(self, chat_id, chat_type, chat_info_json):
        logger.info('create chat {}'.format(chat_id))
        created = self.storage.update_chat_info(chat_id=chat_id,
                                                chat_type=chat_type,
                                                chat_info_json=chat_info_json)
        # TODO - optimize this
        if created:
            return self.get_chat(chat_id)
        result = self.storage.get_chat_info(chat_id)
        logger.info('chat {} queried from storage'.format(chat_id))
        if result is None:
            return None
        chat = BotChat(result)
        self.chats[chat_id] = chat
        return chat

    def update_chat_settings(self, chat: BotChat, settings_json):
        if chat is None or chat.chat_id < 0:
            return None
        if settings_json is None:
            raise Exception('chat settings must be not null')
        self.storage.update_chat_settings(chat_id=chat.chat_id, settings_json=settings_json)
        logger.info('chat {} settings updated {}'.format(chat, settings_json))
        result = self.storage.get_chat_info(chat.chat_id)
        if result is None:
            return None
        chat = BotChat(result)
        self.chats[chat.chat_id] = chat
        return chat

    def update_chat_eth_miner_token(self, chat: BotChat, eth_miner_token):
        if chat is None or chat.chat_id < 0:
            return None
        if eth_miner_token is None:
            raise Exception('chat settings must be not null')
        self.storage.update_eth_miner_token(chat_id=chat.chat_id, eth_miner_token=eth_miner_token)
        logger.info('chat {} token updated {}'.format(chat, eth_miner_token))
        result = self.storage.get_chat_info(chat.chat_id)
        if result is None:
            return None
        chat = BotChat(result)
        self.chats[chat.chat_id] = chat
        return chat
