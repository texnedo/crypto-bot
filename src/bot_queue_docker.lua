box.cfg {
    listen = 3301,
    log_level = 6,
    log = 'tarantool_docker.log'
}

queue = require('queue')
log = require('log')
existing = queue.tube('action_queue')

local function otc_cb(task, stats_data)
    if stats_data == 'delete' then
        log.info("Task %s is done", task)
    end
end

if not existing then
    existing = queue.create_tube('action_queue', 'fifottl', {temporary = false, on_task_change = otc_cb})
end
queue.statistics('action_queue')
