from utils.init_helper import get_logger
from crypto_rest_api import get_crypto_listings
from crypto_rest_api import get_crypto_price

logger = get_logger('crypto_price_provider')


class CryptoPriceItem:
    def __init__(self, data, crypto_name, fiat_name):
        self.fiat_name = fiat_name
        self.crypto_name = crypto_name
        self.price = data['price']
        self.volume_24h = data['volume_24h']
        self.percent_change_1h = data['percent_change_1h']
        self.percent_change_24h = data['percent_change_24h']
        self.percent_change_7d = data['percent_change_7d']
        if self.price < 0 or self.volume_24h < 0:
            raise Exception('invalid price values')

    def write_to_report(self, report_stream):
        if report_stream is None:
            return
        report_stream.write('\nExchange rate {} => {}: {:.2f}'
                            .format(self.crypto_name, self.fiat_name, self.price))
        volatility_format = \
            '\nVolatility    {} => {}:' \
            '\n\t\t\t1 hour: {}' \
            '\n\t\t\t1 day: {}' \
            '\n\t\t\t7 days: {}'
        report_stream.write(
            volatility_format.format(self.crypto_name, self.fiat_name, self.percent_change_1h,
                                     self.percent_change_24h, self.percent_change_7d)
        )


class CryptoPriceHolder:
    def __init__(self, name, ticker_id) -> None:
        super().__init__()
        self.name = name
        self.ticker_id = ticker_id
        self.price_by_fiat_name = {}

    def get_price(self, fiat_name):
        price_for_fiat = self.price_by_fiat_name.get(fiat_name)
        if price_for_fiat is None:
            price_info_data = get_crypto_price(ticker_id=self.ticker_id, fiat_name=fiat_name)
            if price_info_data is None:
                raise Exception('failed to fetch crypto {} price info'.format(self.name))
            data = price_info_data['quotes'].get(fiat_name)
            if data is None:
                raise Exception('failed to fetch crypto {} price info for {}'
                                .format(self.name, fiat_name))
            price_item = CryptoPriceItem(data=data,
                                         crypto_name=self.name,
                                         fiat_name=fiat_name)
            self.price_by_fiat_name[fiat_name] = price_item
            if self.price_by_fiat_name.get(CryptoPriceProvider.FIAT_USD) is None:
                data_usd = price_info_data['quotes'].get(CryptoPriceProvider.FIAT_USD)
                if data_usd is not None:
                    price_item_usd = CryptoPriceItem(data=data_usd,
                                                     crypto_name=self.name,
                                                     fiat_name=CryptoPriceProvider.FIAT_USD)
                    self.price_by_fiat_name[CryptoPriceProvider.FIAT_USD] = price_item_usd
            price_for_fiat = price_item
        return price_for_fiat


class CryptoPriceProvider:
    CRYPTO_ETHERIUM = "ETH"
    CRYPTO_BITCOIN = "BTC"
    FIAT_USD = "USD"
    FIAT_RUR = "RUB"
    FIAT_EUR = "EUR"

    def __init__(self) -> None:
        self.currency_symbol_to_meta = None
        self.currency_symbol_to_price = {}

    def get_price(self, crypto_name, fiat_name):
        if crypto_name is None or len(crypto_name) == 0:
            raise Exception('invalid crypto name')

        crypto_name_upper = crypto_name.upper()
        fiat_name_upper = fiat_name.upper()

        if self.currency_symbol_to_meta is None:
            logger.info("initialize crypto dict")
            self.currency_symbol_to_meta = {}
            listings = get_crypto_listings()
            logger.info("received listings ({})".format(len(listings)))
            for item in listings:
                self.currency_symbol_to_meta[item['symbol'].upper()] = item
            logger.debug("crypto dict initialized ({})".format(len(listings)))

        meta = self.currency_symbol_to_meta.get(crypto_name_upper)
        if meta is None:
            raise Exception('invalid crypto name (not found)')

        price_holder = self.currency_symbol_to_price.get(crypto_name_upper)
        if price_holder is None:
            price_holder = CryptoPriceHolder(crypto_name_upper, meta['id'])
            self.currency_symbol_to_price[crypto_name_upper] = price_holder

        return price_holder.get_price(fiat_name_upper)
