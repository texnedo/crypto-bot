import requests
from utils.init_helper import get_logger

logger = get_logger('crypto_api')


class ApiError(Exception):
    """ Base class API errors. """

    def __init__(self, *args, **kwargs):  # real signature unknown
        pass

    @staticmethod  # known case of __new__
    def __new__(*args, **kwargs):  # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


def get_crypto_listings():
    logger.debug('get_crypto_listings started')
    response = requests.get('https://api.coinmarketcap.com/v2/listings/')
    logger.debug('get_crypto_listings completed with status: %d', response.status_code)
    if response.status_code != 200:
        raise ApiError(response.status_code)
    listings = response.json()
    # TODO - return also timestamp to understand expiration time of response
    return validate_response(listings['data'])


def get_crypto_global():
    logger.debug('get_crypto_global started')
    response = requests.get('https://api.coinmarketcap.com/v2/global/')
    logger.debug('get_crypto_global completed with status: %d', response.status_code)
    if response.status_code != 200:
        raise ApiError(response.status_code)
    listings = response.json()
    return validate_response(listings['data'])


def get_crypto_price(ticker_id, fiat_name):
    logger.debug('get_crypto_price started ({}, {})'.format(ticker_id, fiat_name))
    response = requests.get('https://api.coinmarketcap.com/v2/ticker/{}/?convert={}'
                            .format(ticker_id, fiat_name))
    logger.debug('get_crypto_price completed with status: %d', response.status_code)
    if response.status_code != 200:
        raise ApiError(response.status_code)
    listings = response.json()
    return validate_response(listings['data'])


def get_crypto_top_prices(top_number):
    logger.debug('get_crypto_top_prices started: {}'.format(top_number))
    response = requests.get(
        'https://api.coinmarketcap.com/v2/ticker/?limit={}&sort=rank'.format(top_number))
    logger.debug('get_crypto_top_prices completed with status: %d', response.status_code)
    if response.status_code != 200:
        raise ApiError(response.status_code)
    listings = response.json()
    return validate_response(listings['data'])


def get_ethermine_miner_stats(miner_token):
    logger.debug('get_ethermine_miner_stats started')
    response = requests.get('https://api.ethermine.org/miner/{}/currentStats'.format(miner_token))
    logger.debug('get_ethermine_miner_stats completed with status: %d', response.status_code)
    if response.status_code != 200:
        raise ApiError(response.status_code)
    listings = response.json()
    return validate_response(listings['data'])


def get_ether_address_transactions(miner_token, limit):
    logger.debug('get_ether_address_transactions started: {}'.format(miner_token))
    response = requests.get(
        'http://api.ethplorer.io/getAddressTransactions/{}?apiKey=freekey&limit={}'
            .format(miner_token, limit))
    logger.debug('get_ether_address_transactions completed with status: %d', response.status_code)
    if response.status_code != 200:
        raise ApiError(response.status_code)
    listings = response.json()
    return validate_response(listings)


def get_ether_address_info(miner_token):
    logger.debug('get_ether_address_transactions started: {}'.format(miner_token))
    response = requests.get(
        'http://api.ethplorer.io/getAddressInfo/{}?apiKey=freekey'.format(miner_token))
    logger.debug('get_ether_address_transactions completed with status: %d', response.status_code)
    if response.status_code != 200:
        raise ApiError(response.status_code)
    listings = response.json()
    return validate_response(listings)


def validate_response(response):
    if response is None:
        raise ApiError('empty response')
    return response
