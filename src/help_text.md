*Hello, my name is CryptoBot!*

I can help you to navigate through different cryptocurrency values and exchange rates. Feel free to ask me or check out what I can do for you.

Here is the list of commands:
1. top10 - get to 10 cryptocurrencies rates
2. help - show this tutorial
3. start - start dialog from the beginning
4. get - retrieve cryptocurrency rate by it's name (could be found using command *list*)
5. list - return all supported cryptocurrency names
6. report - show current stats about your etherium miner
7. settings - show settings
   * set target currency (ex: *set_currency RUB*)
   * enable/disable automatic report (ex: *enable auto report*)
   * set automatic report interval in minutes (ex: *auto report interval 10*)
   * set etherium miner token for monitoring and reporting (ex: *set token 0x5423453434952834989234859345834985*)
