from utils.init_helper import get_config
from utils.init_helper import get_logger
from utils.init_helper import set_signal_handler
from datetime import date, datetime, timedelta
import MySQLdb

from utils.utils import read_from_file

logger = get_logger('bot_storage')
config = get_config()
storage_config = config['bot_storage']
db_config = storage_config['mysql_db_credentials']
db_connections = set()


def signal_handler(signum, frame):
    logger.log('handle signal: {} with frame: {}, connections: {}'
               .format(signum, frame, len(db_connections)))
    for db in db_connections:
        db.close()


set_signal_handler(signal_handler)


class BotStorage:
    def __init__(self) -> None:
        self.db = None
        pass

    def __del__(self):
        self.stop()

    def stop(self):
        if self.db is not None:
            db_connections.remove(self.db)
            logger.info('close connection to db')
            self.db.close()
            self.db = None

    def update_chat_info(self, chat_id, chat_type, chat_info_json):
        logger.info('update chat info for {} with {}, {}'
                    .format(chat_id, chat_type, chat_info_json))
        db = self.get_connection()
        cursor = db.cursor()
        current_time = datetime.now()
        affected_rows = 0
        created = False
        try:
            statement = 'insert into bot_chats (chat_id, chat_type, created_ts, last_active_ts, ' \
                        'chat_info_json, settings_json) ' \
                        'values (%s, %s, %s, %s, %s, %s);'
            data = (chat_id, chat_type, current_time, current_time, chat_info_json, '{}')
            cursor.execute(statement, data)
            affected_rows = db.affected_rows()
            db.commit()
            created = True
        except MySQLdb.Error as ex:
            logger.info('failed to insert chat info for {} due to {}'.format(chat_id, ex))
            statement = 'update bot_chats set chat_type = %s, last_active_ts = %s, ' \
                        'chat_info_json = %s where chat_id = %s;'
            data = (chat_type, current_time, chat_info_json, chat_id)
            cursor.execute(statement, data)
            affected_rows = db.affected_rows()
            db.commit()
        if affected_rows != 1:
            raise Exception('rows affected during update: {}'.format(affected_rows))
        cursor.close()
        logger.info('update chat {} completed'.format(chat_id))
        return created

    def update_chat_settings(self, chat_id, settings_json):
        logger.info('update chat info for {} with {}'.format(chat_id, settings_json))
        db = self.get_connection()
        cursor = db.cursor()
        current_time = datetime.now()
        affected_rows = 0
        try:
            statement = 'update bot_chats set settings_json = %s, ' \
                        'last_active_ts = %s where chat_id = %s;'
            data = (settings_json, current_time, chat_id)
            cursor.execute(statement, data)
            affected_rows = db.affected_rows()
            db.commit()
        except MySQLdb.Error as ex:
            logger.error('failed to update chat {} due to {}'.format(chat_id, ex))
            db.rollback()
            raise ex
        if affected_rows != 1:
            raise Exception('rows affected during update: {}'.format(affected_rows))
        cursor.close()
        logger.info('update chat {} completed'.format(chat_id))

    def update_eth_miner_token(self, chat_id, eth_miner_token):
        logger.info('update chat info for {} with {}'.format(chat_id, eth_miner_token))
        db = self.get_connection()
        cursor = db.cursor()
        current_time = datetime.now()
        affected_rows = 0
        try:
            statement = 'update bot_chats set eth_miner_token = %s, last_active_ts = %s' \
                        'where chat_id = %s;'
            data = (eth_miner_token, current_time, chat_id)
            cursor.execute(statement, data)
            affected_rows = db.affected_rows()
            db.commit()
        except MySQLdb.Error as ex:
            logger.error('failed to update chat {} due to {}'.format(chat_id, ex))
            db.rollback()
            raise ex
        if affected_rows != 1:
            raise Exception('rows affected during update: {}'.format(affected_rows))
        cursor.close()
        logger.info('update chat {} completed'.format(chat_id))

    def get_chat_info(self, chat_id):
        logger.info('query chat for {}'.format(chat_id))
        db = self.get_connection()
        cursor = db.cursor()
        statement = 'select * from bot_chats where chat_id = %s;'
        data = [chat_id]
        cursor.execute(statement, data)
        result = cursor.fetchall()
        if len(result) > 1:
            raise Exception('failed to query for {} as there are more than one record'
                            .format(chat_id))
        cursor.close()
        if result is None or len(result) == 0:
            logger.info('info for chat {} not found'.format(chat_id))
            return None
        else:
            logger.info('query chat {} completed with {}'.format(chat_id, result[0]))
            return result[0]

    def get_chat_info_by_token(self, eth_miner_token):
        logger.info('query chat for {}'.format(eth_miner_token))
        db = self.get_connection()
        cursor = db.cursor()
        statement = 'select * from bot_chats where eth_miner_token = %s;'
        data = [eth_miner_token]
        cursor.execute(statement, data)
        result = cursor.fetchall()
        if len(result) > 1:
            raise Exception('failed to query for {} as there are more than one record'
                            .format(eth_miner_token))
        cursor.close()
        if result is None or len(result) == 0:
            logger.info('info for chat {} not found'.format(eth_miner_token))
            return None
        else:
            logger.info('query chat {} completed with {}'.format(eth_miner_token, result[0]))
            return result[0]

    def remove_chat_info(self, chat_id):
        logger.info('remove chat for {}'.format(chat_id))
        db = self.get_connection()
        cursor = db.cursor()
        statement = 'delete from bot_chats where chat_id = %s;'
        data = [chat_id]
        cursor.execute(statement, data)
        result = cursor.fetchall()
        if len(result) > 1:
            raise Exception('failed to remove for {} as there are more than one record'
                            .format(chat_id))
        affected_rows = db.affected_rows()
        if affected_rows > 1:
            raise Exception('rows affected during delete: {}'.format(affected_rows))
        cursor.close()
        logger.info('remove chat {} completed (affected: {})'.format(chat_id, affected_rows))

    def get_connection(self):
        if self.db is None:
            logger.info('create connection to db at {}:{}'
                        .format(db_config['host'], db_config['port']))
            self.db = MySQLdb.connect(
                host=db_config['host'],
                user=db_config['user'],
                port=db_config['port'],
                passwd=db_config['password'],
                db=db_config['db_name']
            )
            db_connections.add(self.db)
            self.run_init_script()
        return self.db

    def run_init_script(self):
        query = read_from_file(storage_config['init_script_file_name'])
        logger.info('run init script query %s on db %s', query, self.db)
        self.db.query(query)
        result = self.db.store_result()
        logger.info('init script db %s result %s', self.db, result)

