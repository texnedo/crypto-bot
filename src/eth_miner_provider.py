from utils.init_helper import get_logger, get_config
from crypto_rest_api import get_ethermine_miner_stats
from datetime import datetime
from eth_token_info_provider import is_valid_eth_token
import time

logger = get_logger('eth_miner_stats_provider')
config = get_config()
provider_config = config['eth_miner_stats_provider']

NOT_AVAILABLE = 'NO DATA'


class EthMinerInfo:
    def __init__(self, data):
        self.init_defaults()
        try:
            if NOT_AVAILABLE == data:
                raise Exception('data from API not available')
            self.time = data['time']
            self.last_seen = data['lastSeen']
            self.reported_hashrate = data['reportedHashrate']
            self.current_hashrate = data['currentHashrate']
            self.valid_shares = data['validShares']
            self.invalid_shares = data['invalidShares']
            self.stale_shares = data['staleShares']
            self.average_hashrate = data['averageHashrate']
            self.active_workers = data['activeWorkers']
            self.unpaid = data['unpaid']
            self.unconfirmed = data['unconfirmed']
            self.coins_perMin = data['coinsPerMin']
            self.usd_per_min = data['usdPerMin']
            self.btc_per_min = data['btcPerMin']
            incorrect_format = False
            if self.time is not None and self.time < 0:
                incorrect_format = True
            if self.active_workers is not None and self.active_workers < 0:
                incorrect_format = True
            if self.last_seen is not None and self.last_seen < 0:
                incorrect_format = True
            if self.reported_hashrate is not None and self.reported_hashrate < 0:
                incorrect_format = True
            if self.current_hashrate is not None and self.current_hashrate < 0:
                incorrect_format = True
            if incorrect_format:
                raise Exception('invalid miner stats API response format')
        except Exception as ex:
            self.time = int(time.time())
            logger.error('failed to parse API response due to an error: %s', ex)
        self.last_updated_time = int(time.time())

    def init_defaults(self):
        self.time = None
        self.last_seen = None
        self.reported_hashrate = None
        self.current_hashrate = None
        self.valid_shares = None
        self.invalid_shares = None
        self.stale_shares = None
        self.average_hashrate = None
        self.active_workers = None
        self.unpaid = None
        self.unconfirmed = None
        self.coins_perMin = None
        self.usd_per_min = None
        self.btc_per_min = None

    def is_expired(self):
        current = int(time.time())
        diff = current - self.last_updated_time
        if diff < 0 or diff > provider_config['cache_timeout']:
            return True
        return False

    def write_to_report(self, report_stream):
        if report_stream is None:
            return
        time_string = datetime.fromtimestamp(self.time).strftime('%Y-%m-%d %H:%M:%S')
        report_stream.write('\nLast updated: {}'.format(time_string))
        if self.last_seen is not None:
            last_seen_string = datetime.fromtimestamp(self.last_seen).strftime('%Y-%m-%d %H:%M:%S')
            report_stream.write('\nLast seen: {}'.format(last_seen_string))
        else:
            report_stream.write('\nLast seen: unavailable')
        if self.reported_hashrate is not None:
            report_stream.write('\nReported hashrate: {:.2f} Mh/s'
                                .format(self.reported_hashrate / 1000000))
        else:
            report_stream.write('\nReported hashrate: unavailable')
        if self.current_hashrate is not None:
            report_stream.write('\nCurrent hashrate: {:.2f} Mh/s'
                                .format(self.current_hashrate / 1000000))
        else:
            report_stream.write('\nCurrent hashrate: unavailable')
        if self.average_hashrate is not None:
            report_stream.write('\nAverage hashrate: {:.2f} Mh/s'
                                .format(self.average_hashrate / 1000000))
        else:
            report_stream.write('\nAverage hashrate: unavailable')
        if self.active_workers is not None:
            report_stream.write('\nActive workers: {}'.format(self.active_workers))
        else:
            report_stream.write('\nActive workers: unavailable')


class EthMinerStatsProvider:
    def __init__(self) -> None:
        self.token_to_miner_stats = {}

    def get_miner_stats(self, token):
        if token is None or not is_valid_eth_token(token):
            raise Exception('incorrect token format')
        miner_stats = self.token_to_miner_stats.get(token)
        if miner_stats is None or miner_stats.is_expired():
            miner_stats_data = get_ethermine_miner_stats(miner_token=token)
            miner_stats = EthMinerInfo(miner_stats_data)
            self.token_to_miner_stats[token] = miner_stats
        return miner_stats
