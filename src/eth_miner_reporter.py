from utils.init_helper import get_logger
from eth_miner_provider import EthMinerStatsProvider
from eth_token_info_provider import EthTokenInfoProvider
from crypto_price_provider import CryptoPriceProvider
import io

logger = get_logger('eth_miner_reporter')


class EthMinerReporter:
    def __init__(self):
        self.miner_stats = EthMinerStatsProvider()
        self.price_provider = CryptoPriceProvider()
        self.token_provider = EthTokenInfoProvider()

    def make_text_report(self, token):
        balance = self.token_provider.get_token_balance(token)
        stats = self.miner_stats.get_miner_stats(token)
        price = self.price_provider.get_price(crypto_name=CryptoPriceProvider.CRYPTO_ETHERIUM,
                                              fiat_name=CryptoPriceProvider.FIAT_RUR)

        report = io.StringIO()
        report.write('\n===============================')
        stats.write_to_report(report)
        report.write('\n===============================')
        price.write_to_report(report)
        report.write('\n===============================')
        fiat_names = [CryptoPriceProvider.FIAT_RUR, CryptoPriceProvider.FIAT_USD]
        balance.write_to_report(report_stream=report,
                                price_provider=self.price_provider,
                                convert_to_fiat_name=fiat_names)
        report.write('\n===============================')
        return report.getvalue()
