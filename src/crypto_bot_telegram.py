import io
from threading import Event, current_thread
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from eth_miner_reporter import EthMinerReporter
from bot_chats_manager import BotChatsManager
from bot_storage import BotStorage
from eth_token_info_provider import is_valid_eth_token
from utils.utils import obj2json, json2obj
from scheduled_action_manager import ScheduledActionManager, ChatActionType, ScheduledActionType
from scheduled_action_manager import ServiceAction
from scheduled_action_manager import ChatAction
from scheduled_action_manager import ActionSubscriber
import telegram.vendor.ptb_urllib3.urllib3
import telegram
from utils.init_helper import get_logger, get_config, set_signal_handler
import logging
import telegram.utils.request
import crypto_rest_api
import help_loader
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from datetime import datetime
from utils.utils import eth_token_from_hash
from utils.init_helper import install_proxy_rewrite

# read bot configuration
config = get_config()
telegram_config = config['telegram_config']
bot_config = config['crypto_bot_config']
logger = get_logger('crypto_bot')

# set up proxy rewrite handler (as Telegram api is blocked currently in some locations)
install_proxy_rewrite(
    host=telegram_config['api_host'],
    ip=telegram_config['api_gateway_ip']
)


class CryptoBot(ActionSubscriber):

    def __init__(self):
        request_kwargs = bot_config['request_kwargs']
        proxy_request = telegram.utils.request.Request(proxy_url=request_kwargs['proxy_url'],
                                                       urllib3_proxy_kwargs=request_kwargs[
                                                           'urllib3_proxy_kwargs'],
                                                       connect_timeout=30,
                                                       read_timeout=30)
        self.is_bot_started = False
        self.event_stop = Event()
        self.bot = telegram.Bot(token=telegram_config['bot_token'], request=proxy_request)
        self.bot.logger = logger
        self.reporter = EthMinerReporter()
        self.bot_storage = BotStorage()
        self.chats_manager = BotChatsManager(self.bot_storage)
        self.action_manager = ScheduledActionManager()
        self.updater = Updater(request_kwargs=request_kwargs, bot=self.bot)
        self.updater.logger = logger
        self.dispatcher = self.updater.dispatcher
        self.dispatcher.logger = logger
        self.dispatcher.add_handler(CommandHandler('start', self.start_handler))
        self.dispatcher.add_handler(CommandHandler('top10', self.top10_handler))
        self.dispatcher.add_handler(CommandHandler('help', self.help_handler))
        self.dispatcher.add_handler(CommandHandler('get', self.get_handler))
        self.dispatcher.add_handler(CommandHandler('list', self.list_handler))
        self.dispatcher.add_handler(CommandHandler('report', self.report_handler))
        self.dispatcher.add_handler(CommandHandler('settings', self.settings_handler))
        self.dispatcher.add_handler(
            MessageHandler(
                filters=Filters.text,
                callback=self.echo_handler,
                pass_chat_data=True,
                pass_user_data=True
            )
        )
        self.dispatcher.add_handler(CallbackQueryHandler(self.set_eth_miner_token,
                                                         pattern='set_eth_miner_token'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.set_currency,
                                                         pattern='set_currency'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.set_report_interval,
                                                         pattern='set_report_interval'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.enable_auto_report,
                                                         pattern='enable_auto_report'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.show_saved_settings,
                                                         pattern='show_saved_settings'))
        self.dispatcher.add_error_handler(self.error_handler)

    def process_action(self, action):
        logger.info('Process scheduled action: %s', action)
        if action is None:
            return
        if action.type == ScheduledActionType.CHAT:
            return self.process_chat_action(action)
        return False

    def process_chat_action(self, action):
        try:
            chat = self.chats_manager.get_chat(action.chat_id)
            settings = json2obj(chat.settings_json)
            auto_report = settings.get('enable_auto_report')
            if auto_report:
                if is_valid_eth_token(chat.eth_miner_token):
                    timeout = self.get_current_report_timeout(action=action, settings=settings)
                    if timeout is None or timeout < 0:
                        raise Exception('Incorrect report timeout: {}'.format(timeout))
                    if timeout == 0:
                        logger.info('Show report for action %s as timeout expired', action)
                        self.bot.send_message(
                            chat_id=action.chat_id,
                            text=self.reporter.make_text_report(chat.eth_miner_token)
                        )
                        settings['last_report_time'] = datetime.now().timestamp()
                        self.chats_manager.update_chat_settings(chat, obj2json(settings))
                        timeout = self.get_current_report_timeout(action=action, settings=settings)
                    logger.info('Next report for action %s will be fired after %d', action, timeout)
                    self.schedule_eth_miner_report(chat=chat, report_interval=timeout)
                else:
                    self.bot.send_message(chat_id=action.chat_id,
                                          text='Etherium miner token is not configured currently. '
                                               'Use /settings to configure it.')
                    settings['enable_auto_report'] = 0
                    self.chats_manager.update_chat_settings(chat, obj2json(settings))
                    self.bot.send_message(chat_id=action.chat_id,
                                          text='Auto report disabled')
            else:
                logger.info('Auto miner report has been disabled for chat %s', action.chat_id)
            return True
        except Exception as ex:
            logger.error('Failed to process scheduled action %s due to %s', action, ex)
            self.bot.send_message(chat_id=action.chat_id,
                                  text='Can\'t process scheduled action for this chat, '
                                       'we will fix this soon!')
            return False

    def get_report_timeout(self, action, settings):
        report_interval = settings.get('report_interval')
        if report_interval is None:
            logger.info('Report interval is not provided for action: %s', action)
            report_interval = telegram_config['default_report_interval']
        return report_interval

    def get_current_report_timeout(self, action, settings):
        report_interval = settings.get('report_interval')
        last_report_time = settings.get('last_report_time')
        if last_report_time is None:
            logger.info('No last report for action: %s', action)
            return 0

        if report_interval is None:
            logger.info('Report interval is not provided for action: %s', action)
            report_interval = telegram_config['default_report_interval']

        current_report_time = datetime.now().timestamp()
        diff = current_report_time - last_report_time
        if diff < 0 or report_interval < 0:
            logger.error(
                'Failed to show report due to incorrectly '
                'provided time (current: %d, last: %d)',
                current_report_time, last_report_time
            )
            return report_interval
        if diff > report_interval:
            return 0
        return report_interval - diff

    def start_handler(self, bot, update):
        data = crypto_rest_api.get_crypto_global()
        update.message.reply_text(('Active cryptocurrencies: {} \n' +
                                   'Active markets: {} \n' +
                                   'Bitcoin percentage of market cap: {}% \n' +
                                   'Total market cap: $ {:10.2f}M \n' +
                                   'Total volume 24h: $ {:10.2f}M \n' +
                                   'Last updated: {}')
                                  .format(data['active_cryptocurrencies'],
                                          data['active_markets'],
                                          data['bitcoin_percentage_of_market_cap'],
                                          data['quotes']['USD']['total_market_cap'] / 1000000,
                                          data['quotes']['USD']['total_volume_24h'] / 1000000,
                                          datetime.fromtimestamp(data['last_updated'])
                                          )
                                  )

    def help_handler(self, bot, update):
        update.message.reply_markdown(help_loader.get_help_text())

    def top10_handler(self, bot, update):
        update.message.reply_text('Show top10!')

    def get_handler(self, bot, update):
        update.message.reply_text('Get!')

    def list_handler(self, bot, update):
        update.message.reply_text('List!')

    def report_handler(self, bot, update):
        message = update.message
        try:
            chat = self.chats_manager.update_chat(chat_id=message.chat_id,
                                                  chat_type=message.chat.type,
                                                  chat_info_json=message.chat)
            if is_valid_eth_token(chat.eth_miner_token):
                message.reply_text(self.reporter.make_text_report(chat.eth_miner_token))
                self.schedule_next_report(chat)
            else:
                message.reply_text('Etherium miner token is not configured yet. '
                                   'Use /settings to configure it.')
        except Exception as ex:
            logger.error('Failed to build report update due to %s', ex)
            message.reply_text('Can\'t build your report right now, we will fix this soon!')

    def settings_handler(self, bot, update):
        update.message.reply_text(self.settings_menu_message(),
                                  reply_markup=self.settings_menu_keyboard())

    def settings_menu_message(self):
        return 'Settings menu:'

    def settings_menu(self, bot, update):
        query = update.callback_query
        bot.edit_message_text(chat_id=query.message.chat_id,
                              message_id=query.message.message_id,
                              text=self.settings_menu_message(),
                              reply_markup=self.settings_menu_keyboard())

    def settings_menu_keyboard(self):
        keyboard = [[InlineKeyboardButton('Set target currency', callback_data='set_currency')],
                    [InlineKeyboardButton('Enable/Disable automatic report',
                                          callback_data='enable_auto_report')],
                    [InlineKeyboardButton('Set report interval in seconds',
                                          callback_data='set_report_interval')],
                    [InlineKeyboardButton('Set etherium miner token',
                                          callback_data='set_eth_miner_token')],
                    [InlineKeyboardButton('Show saved settings',
                                          callback_data='show_saved_settings')],
                    ]
        return InlineKeyboardMarkup(keyboard)

    def show_saved_settings(self, bot, update):
        message = update.callback_query.message
        try:
            logger.info('Query settings from update %s', update)
            chat = self.chats_manager.get_chat(chat_id=message.chat_id)
            settings = json2obj(chat.settings_json)
            settings_report = 'User settings:\nEtherium miner token: {}\nTarget fiat currency: {}\n' \
                              'Auto report enabled: {}\nAuto report interval (seconds): {}' \
                .format(chat.eth_miner_token,
                        settings.get('currency'),
                        'Yes' if settings.get('enable_auto_report') == 1 else 'No',
                        settings.get('report_interval')
                        )
            message.reply_text(settings_report)
        except Exception as ex:
            logger.error('Failed to show saved settings due to %s', ex)
            message.reply_text('Can\'t show your saved settings, we will fix this soon!')

    def set_eth_miner_token(self, bot, update):
        message = update.callback_query.message
        try:
            logger.info('Update message %s with set_eth_miner_token', update)
            chat = self.chats_manager.update_chat(chat_id=message.chat_id,
                                                  chat_type=message.chat.type,
                                                  chat_info_json=message.chat)
            logger.info('Chat %s updated with data %s', chat.chat_id, update)
            message.reply_text(
                'Enter Etherium miner token in format: 0x5423453434952834989234859345834985'
            )
            settings = json2obj(chat.settings_json)
            settings['last_received_command'] = 'set_eth_miner_token'
            self.chats_manager.update_chat_settings(chat, obj2json(settings))
        except Exception as ex:
            logger.error('Failed to process set_eth_miner_token due to %s', ex)
            message.reply_text('Can\'t update your Etherium miner token, we will fix this soon!')

    def set_currency(self, bot, update):
        message = update.callback_query.message
        try:
            logger.info('Update message %s with set_currency', update)
            chat = self.chats_manager.update_chat(chat_id=message.chat_id,
                                                  chat_type=message.chat.type,
                                                  chat_info_json=message.chat)
            logger.info('Chat %s updated with data %s', chat.chat_id, update)
            reply_text = 'Enter desired fiat currency to convert crypto currency (ex: RUR, EUR, ' \
                         'USD...) '
            settings = json2obj(chat.settings_json)
            if settings.get('currency'):
                reply_text += '\nLast used currency is {}'.format(reply_text)
            message.reply_text(reply_text)

            settings = json2obj(chat.settings_json)
            settings['last_received_command'] = 'set_currency'
            self.chats_manager.update_chat_settings(chat, obj2json(settings))
        except Exception as ex:
            logger.error('Failed to process set_currency due to %s', ex)
            message.reply_text('Can\'t update your currency, we will fix this soon!')

    def set_report_interval(self, bot, update):
        message = update.callback_query.message
        try:
            logger.info('Update message %s with set_report_interval', update)
            chat = self.chats_manager.update_chat(chat_id=message.chat_id,
                                                  chat_type=message.chat.type,
                                                  chat_info_json=message.chat)
            logger.info('Chat %s updated with data %s', chat.chat_id, update)
            message.reply_text(
                'Enter auto report time interval in seconds'
            )
            settings = json2obj(chat.settings_json)
            settings['last_received_command'] = 'set_report_interval'
            self.chats_manager.update_chat_settings(chat, obj2json(settings))
            self.schedule_next_report(chat)
        except Exception as ex:
            logger.error('Failed to process set_report_interval due to %s', ex)
            message.reply_text('Can\'t update your auto report interval, we will fix this soon!')

    def enable_auto_report(self, bot, update):
        message = update.callback_query.message
        try:
            logger.info('Update message %s with enable_auto_report', update)
            chat = self.chats_manager.update_chat(chat_id=message.chat_id,
                                                  chat_type=message.chat.type,
                                                  chat_info_json=message.chat)
            logger.info('Chat %s updated with data %s', chat.chat_id, update)
            settings = json2obj(chat.settings_json)
            auto_report = settings.get('enable_auto_report')
            report_interval = settings.get('report_interval')
            default_interval_used = False
            if report_interval is None or report_interval < 0:
                report_interval = telegram_config['default_report_interval']
                default_interval_used = True
            if auto_report == 1:
                message.reply_text('Auto report disabled')
                settings['enable_auto_report'] = 0
                self.chats_manager.update_chat_settings(chat, obj2json(settings))
            else:
                message.reply_text('Auto report enabled with interval {} (default: {})'
                                   .format(report_interval, default_interval_used))
                settings['enable_auto_report'] = 1
                self.chats_manager.update_chat_settings(chat, obj2json(settings))
                self.schedule_eth_miner_report(chat=chat, report_interval=report_interval)
        except Exception as ex:
            logger.error('Failed to process enable_auto_report due to %s', ex)
            message.reply_text('Can\'t change your auto report preference, we will fix this soon!')

    def schedule_eth_miner_report(self, chat, report_interval):
        if chat is None or report_interval < 0:
            logger.error('Failed to schedule eth miner report for chat %s and interval %s',
                         chat, report_interval)
            return
        report_action = ChatAction()
        report_action.chat_action_type = ChatActionType.MINER_REPORT
        report_action.delay = report_interval
        report_action.chat_id = chat.chat_id
        self.action_manager.add_action(report_action)
        logger.info('Next eth miner report for %s scheduled in %s', chat.chat_id, report_interval)

    def echo_handler(self, bot, update, user_data, chat_data):
        logger.info('Update message %s from user %s with chat data %s', update, user_data,
                    chat_data)
        message = update.message
        try:
            chat = self.chats_manager.update_chat(chat_id=message.chat_id,
                                                  chat_type=message.chat.type,
                                                  chat_info_json=message.chat)
            settings = json2obj(chat.settings_json)
            last_command = settings.get('last_received_command')
            logger.info('Process user command %s response', last_command)
            # TODO - use switch case logic
            if last_command == 'set_eth_miner_token':
                self.process_set_eth_miner_response(chat, message, settings)
            elif last_command == 'set_report_interval':
                self.process_set_report_interval_response(chat, message, settings)
            elif last_command == 'set_currency':
                self.process_set_currency_response(chat, message, settings)
        except Exception as ex:
            logger.error('Failed to process user response due to %s', ex)
            message.reply_text('Can\'t process your message, we will fix this soon!')

    def process_set_currency_response(self, chat, message, settings):
        currency = message.text
        success = False
        if currency is not None:
            # TODO - add currency validation
            settings['currency'] = currency
            success = True
        settings.pop('last_received_command', None)
        self.chats_manager.update_chat_settings(chat, obj2json(settings))
        if success:
            message.reply_text('Target fiat currency has been successfully updated')

    def process_set_report_interval_response(self, chat, message, settings):
        interval = message.text
        success = False
        if interval is not None:
            try:
                num_interval = int(interval)
                settings['report_interval'] = num_interval
                success = True
            except ValueError:
                message.reply_text(
                    'Illegal interval format, must be a number of minutes between auto reports'
                )
        settings.pop('last_received_command', None)
        self.chats_manager.update_chat_settings(chat, obj2json(settings))
        if success:
            message.reply_text('Automatic report interval has been successfully updated')

    def process_set_eth_miner_response(self, chat, message, settings):
        if is_valid_eth_token(message.text):
            chat = self.chats_manager.update_chat_eth_miner_token(chat, message.text)
            message.reply_text('Etherium miner token has been successfully updated. '
                               'Use /report to check it''s current state.')
        else:
            message.reply_text('Check your etherium miner token format')
        settings.pop('last_received_command', None)
        self.chats_manager.update_chat_settings(chat, obj2json(settings))

    def schedule_next_report(self, chat):
        settings = json2obj(chat.settings_json)
        auto_report = settings.get('enable_auto_report')
        if auto_report:
            if is_valid_eth_token(chat.eth_miner_token):
                timeout = self.get_current_report_timeout(action=None, settings=settings)
                if timeout is None or timeout < 0:
                    raise Exception('Incorrect report timeout: {}'.format(timeout))
                if timeout == 0:
                    timeout = self.get_report_timeout(action=None, settings=settings)
                logger.info(
                    'Next report for chat %s will be fired after %d (re-scheduled)',
                    chat,
                    timeout
                )
                self.schedule_eth_miner_report(chat=chat, report_interval=timeout)

    def error_handler(self, bot, update, error):
        logger.warning('Update %s caused error %s', update, error)

    def signal_handler(self, signum, frame):
        logger.warning('Handle signal: {} with frame: {}'.format(signum, frame))
        self.event_stop.set()
        self.updater.stop()
        self.action_manager.stop(signum=signum)
        self.bot_storage.stop()

    def start(self):
        if self.is_bot_started:
            raise Exception('Bot has been already started')
        set_signal_handler(self.signal_handler)
        self.action_manager.add_subscriber(self)
        logger.info('Bot started: %s', self.bot.get_me())
        self.updater.start_polling()
        logger.info('Bot main wait loop started')
        self.is_bot_started = True
        while not self.event_stop.isSet():
            if self.event_stop.wait(bot_config['thread_wait_check_event']):
                logger.info('Stop processing in thread by event: %s', current_thread())
                break
        logger.info('Bot main wait loop completed')


crypto_bot = CryptoBot()
crypto_bot.start()
