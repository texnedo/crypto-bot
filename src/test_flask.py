from flask import Flask
from crypto_rest_api import get_crypto_listings
application = Flask(__name__)


@application.route("/getListings")
def get_listings():
    listings = get_crypto_listings()
    return str(listings)


if __name__ == "__main__":
    application.run(host='0.0.0.0')
