CREATE DATABASE IF NOT EXISTS bot_db;

USE bot_db;

CREATE TABLE IF NOT EXISTS `bot_chats` (
  `chat_id` int(11) NOT NULL,
  `chat_type` varchar(30) NOT NULL,
  `created_ts` datetime NOT NULL,
  `last_active_ts` datetime NOT NULL,
  `eth_miner_token` varchar(80) DEFAULT NULL,
  `chat_info_json` text,
  `settings_json` text,
  PRIMARY KEY (`chat_id`),
  UNIQUE KEY `bot_chats_eth_miner_token_index` (`eth_miner_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8