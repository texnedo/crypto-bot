from utils.init_helper import get_logger, get_config
import time
import re
from crypto_rest_api import get_ether_address_info
from crypto_price_provider import CryptoPriceProvider

logger = get_logger('eth_token_provider')
config = get_config()
provider_config = config['eth_token_provider']

ETH_TOKEN_REGEX = re.compile('^0x.{40}$')


class EthTokenInfo:

    def __init__(self, data) -> None:
        self.last_updated_time = int(time.time())
        self.balance = data['ETH']['balance']
        self.transaction_count = data['countTxs']
        if self.balance < 0 or self.transaction_count < 0:
            raise Exception('illegal balance data')

    def is_expired(self):
        current = int(time.time())
        diff = current - self.last_updated_time
        if diff < 0 or diff > provider_config['cache_timeout']:
            return True
        return False

    def write_to_report(self,
                        report_stream,
                        price_provider: CryptoPriceProvider,
                        convert_to_fiat_name: list):
        if report_stream is None or \
                convert_to_fiat_name is None or \
                not isinstance(convert_to_fiat_name, list):
            return
        report_stream.write('\nTransaction count: {}'.format(self.transaction_count))
        if isinstance(convert_to_fiat_name, list):
            for fiat_name in convert_to_fiat_name:
                price_info = price_provider.get_price(
                    crypto_name=CryptoPriceProvider.CRYPTO_ETHERIUM,
                    fiat_name=fiat_name
                )
                if price_info is None:
                    continue
                report_stream.write(
                    '\nBalance: {:.3f} {} => {:.3f} {}'.format(
                        self.balance,
                        CryptoPriceProvider.CRYPTO_ETHERIUM,
                        self.balance * price_info.price,
                        fiat_name
                    )
                )


class EthTokenInfoProvider:

    def __init__(self) -> None:
        self.token_to_balance = {}

    def get_token_balance(self, token):
        if token is None or not is_valid_eth_token(token):
            raise Exception('incorrect token format')
        token_balance = self.token_to_balance.get(token)
        if token_balance is None or token_balance.is_expired():
            token_balance_data = get_ether_address_info(miner_token=token)
            token_balance = EthTokenInfo(token_balance_data)
            self.token_to_balance[token] = token_balance
        return token_balance


def is_valid_eth_token(token):
    if token is None:
        return False
    if len(token) == 0:
        return False
    return ETH_TOKEN_REGEX.match(token) is not None
